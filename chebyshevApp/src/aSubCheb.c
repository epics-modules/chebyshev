#include <dbDefs.h>
#include <dbFldTypes.h>
#include <dbAccess.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <epicsExport.h>
#include <errMdef.h>
#include <errlog.h>
#include "cantProceed.h"
#include <alarm.h>
#include <recGbl.h>
#include <math.h>

epicsUInt32 num_intervals;

epicsFloat64 eval_chebseries ( epicsFloat64 x, epicsFloat64 coef[], epicsInt32 ncoeff, epicsFloat64 min, epicsFloat64 max)
{
    /* Comes from https://people.sc.fsu.edu/~jburkardt/c_src/chebyshev_series/chebyshev_series.html */

    epicsFloat64 b0;
    epicsFloat64 b1 = 0.0;
    epicsFloat64 b2 = 0.0;
    epicsInt32 i;
    epicsFloat64 value;
    epicsFloat64 x2;

    b0 = coef[ncoeff - 1];

    // Scale the input argument to fit between -1 and +1
    x2 = 2.0 * ((x - min) - (max - x))/(max - min);

    for ( i = ncoeff - 2; 0 <= i; i-- )
    {
        b2 = b1;
        b1 = b0;
        b0 = coef[i] - b2 + x2 * b1;
    }

    value = 0.5 * ( b0 - b2 );
    value += coef[0]/2; // Correct the degree 0 term

    return value;
}

static long subChebInit(aSubRecord *prec) {
    epicsUInt32 *pnox, i;
    epicsFloat64 **pmin, **pmax;

    num_intervals = 0;
    pnox = &prec->noc;
    while (*pnox > 1) {
        num_intervals++;
        pnox++;pnox++;
    }

    pmin = &prec->b;
    pmax = &prec->d;
    for (i = 0; i < num_intervals; i++, pmin++, pmin++, pmax++, pmax++) {
        if (**pmax <= **pmin) {
            char message[100];
            sprintf(message, "subChebInit - interval %d is non-increasing (%g, %g)", i, **pmin, **pmax);
            recGblRecordError(S_db_badField, (void *)prec, message);
        }
    }
    return 0;
}

static long subChebProc(aSubRecord *prec) {
    epicsFloat64 inpval = *(epicsFloat64 *)prec->a;

    epicsFloat64 **pcoeffs;
    epicsInt32 *ncoeffs;
    epicsFloat64 **pmin, **pmax;

    epicsUInt32 interval = 0;

    pcoeffs = &prec->c;
    ncoeffs = &prec->nec;
    pmin = &prec->b;
    pmax = &prec->d;
    while(++interval < num_intervals && inpval > **pmax) {
        pcoeffs++;pcoeffs++;
        ncoeffs++;ncoeffs++;
        pmin++;pmin++;
        pmax++;pmax++;
    }
    *(epicsFloat64 *)prec->vala = eval_chebseries(inpval, *pcoeffs, *ncoeffs, **pmin, **pmax);

    return 0;
}

epicsRegisterFunction(subChebInit);
epicsRegisterFunction(subChebProc);