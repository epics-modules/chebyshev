
chebyshev
======
European Spallation Source ERIC Site-specific EPICS module : chebyshev

# Build instructions

This module should compile as a regular EPICS module.
* Run `make` from the command line.

This module should also compile as an E3 module:
* Install `conda` (https://docs.conda.io/en/latest/miniconda.html)
* Create and activate a conda environment that minimally includes the following packages: `make perl tclx compilers epics-base require`
* Run `make -f Makefile.E3` to build the target using the E3 build process

# Definitions

Chebyshev polynomials are defined by the following expression.

`T_n(x) = cos(n * arccos(x))`

which is only valid for values of x such that -1 <= x <= 1. As an example, `cos(2x) = cos(x)^2 - 1` and so `T_2(x) = x^2 - 1`.

These polynomials can be used for approximating unknown (or difficult to calculate) functions, a method we call *Chebyshev approximation*. The idea is the same as any other polynomial approxmation. We choose coefficients c_0, ..., c_n up to some order n, and then evaluate the function

`F(x) = c_0 * T_0(x) + c_1 * T_1(x) + ... + c_n * T_n(x)`

The `chebyshev` module uses an EPICS aSub record in order to calculate this approximation for you. Furthermore, the module allows the user to specify upper and lower bounds to allow you to approximate functions defined on intervals other than [-1, 1].

# Usage

To test this, simply run (in E3)

`iocsh.bash cmds/st_simple.cmd`

and in a separate terminal, try

```
pvput CHEBSIMPLE:input <value>
pvget CHEBSIMPLE:cheb_output
```

To configure this example, modify the following lines in the `aSub` record in aSubChebSimple.db:

```
    field(INPB, "0")

    field(INPC, "[1, 0.5, -2, 1, -1, 3]")

    field(INPD, "100")
```

These define (respectively) the minimum range, the coefficients, and the maximum range for the approximation. In particular, the coefficients above yield

`F(x) = 1 + 0.5 * T_1(x) - 2 * T_2(x) + T_3(x) - T_4(x) + 3 * T_5(x)`

The fields INPB and INPD define the range over which the approximation is assumed to be valid. That is, we should have `INPB <= CHEBSIMPLE:input <= INPD`.

# CERNOX example

We further include a more complicated example which comes from calibrating a CERNOX temperature sensor. 

